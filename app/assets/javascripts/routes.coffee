angular.module('tickets')
.config([
	'$stateProvider',
	'$locationProvider',
	'$urlRouterProvider',
	($stateProvider, $locationProvider, $urlRouterProvider) ->
		$urlRouterProvider.otherwise '/'
		$stateProvider
			.state 'index',
	      url: '/',
	      templateUrl: 'home/index.html',
	      controller: 'HomeController'
     .state 'statisticsindex',
	      url: '/statistics',
	      templateUrl: 'statistics/index.html',
	      controller: 'HomeController',
	  $locationProvider.html5Mode true
])